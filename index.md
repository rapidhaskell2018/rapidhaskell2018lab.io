# RapidHaskell 2018

This is the landing page for Codethink's Rapid Applications Prototyping in
Haskell workshops. The main gitlab group page is
[here](https://gitlab.com/rapidhaskell2018). Feel free to offer merge
requests on this page or raise issues if anything is unclear.

## Before you start

* Make sure you have [stack](https://docs.haskellstack.org/en/stable/README/) installed, and an account on [gitlab.com](https://gitlab.com).
* Make sure you have hlint installed with `stack install hlint`.
* Join #haskell on [irc.freenode.net](https://webchat.freenode.net/).

## Tips

* Try some exercises and make your own notes.
* Pick some videos to watch. It's helpful to see things presented differently.
* Ask questions on #haskell, they are very helpful there.
* Practice, practice, practice.

## Workshop Materials

### Workshop Session 1 (19/07/2018)

* [Source](https://gitlab.com/rapidhaskell2018/ws1)
* [Slides](https://rapidhaskell2018.gitlab.io/workshops/1.pdf)

### Workshop Session 2 (26/07/2018)

* [Source](https://gitlab.com/rapidhaskell2018/ws2)
* [Slides](https://rapidhaskell2018.gitlab.io/workshops/2.pdf)

### Workshop Session 3 (02/08/2018)

* [Source](https://gitlab.com/rapidhaskell2018/ws3)
* [Slides](https://rapidhaskell2018.gitlab.io/workshops/3.pdf)

### Workshop Session 4 (01/10/2018)

* [Source](https://gitlab.com/rapidhaskell2018/ws4)
* [Slides](https://rapidhaskell2018.gitlab.io/workshops/4.pdf)

### Workshop Session 5 (08/10/2018) (Upcoming/In Progress)

* [Slides](https://rapidhaskell2018.gitlab.io/workshops/5.pdf)

### Workshop Session 6 (15/10/2018) (In Progress)


## Handy Links

* [A Nonlinear Guide To Haskell](https://locallycompact.gitlab.io/ANLGTH)
* [Category Theory For Programmers](https://www.youtube.com/playlist?list=PLbgaMIhjbmEnaH_LTkxLI7FMa2HsnawM_)
* [Haskell Weekly](https://haskellweekly.news)
* [Haskell @ Reddit](https://reddit.com/r/haskell)
* [Parallel and Concurrent Haskell](https://www.youtube.com/playlist?list=PLGvfHSgImk4ZN5wqV7GXsZZRug0qNQZh0)
* [Pointfree.io](http://pointfree.io/)
* [Stackage](https://stackage.org)
* [ZenHaskell](http://zenhaskell.gitlab.io/)

## Exercises

* [Exercism](https://exercism.io/my/tracks)
* [Project Euler](https://projecteuler.net/archives)
