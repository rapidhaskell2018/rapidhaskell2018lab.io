{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

import           Data.Aeson.Lens
import           Data.Yaml
import           Development.Shake
import           Development.Shake.Command
import           Development.Shake.FilePath
import           Development.Shake.Util
import           RIO
import qualified RIO.Text                      as T
import           Slick

site = "public"

pandoc input output opts = command [] "pandoc" $ input ++ ["-o", output, "-s"] ++ opts

data Page = Page {
  content :: Text
} deriving (Eq, Show, Generic, FromJSON)


main :: IO ()
main = shakeArgs shakeOptions $ do
  let workshops = site </> "workshops" </> "*.pdf"
  let index     = site </> "index.html"

  want ["index", "slides"]

  phony "clean" $ do
    putNormal $ "Cleaning files in " ++ site
    removeFilesAfter "." [site]

  index %> \out -> do
    let src = dropDirectory1 out -<.> ".md"
    need [src]
    fileContents <- readFile' src
    (page :: Page) <- markdownToHTML' . T.pack $ fileContents
    writeFile' out $ T.unpack (content page)

  workshops %> \out -> do
    let src = replaceDirectory out "slides" -<.> ".md"
    need [src]
    pandoc [src] out ["-t", "beamer", "--variable", "fonttheme:serif"]

  phony "slides"
    $   need
    $   ((site </> "workshops") </>) . (++ ".pdf")
    <$> ["1", "2", "3", "4", "5"]
  phony "index" $ need [index]
